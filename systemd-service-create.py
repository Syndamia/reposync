#!/bin/python3
import os

# This tests root permissions by trying to write a file (it deletes it afterwards)
# "Easer to Ask Forgiveness than permission" https://stackoverflow.com/a/2806932/12036073
try:
    test = open(r"/etc/test", "w")
    test.close();
    os.remove(r"/etc/test")
except PermissionError as e:
    exit("You must run this script as root!")

cwd = os.getcwd()
print("WARNING: if you use SELinux, you'll need to move the whole repository into /usr/local/share/ or copy reposync.py and update the repository list file location (https://serverfault.com/a/957087/592409)!")

service_file = open(r"/etc/systemd/system/reposync.service", "w")
service_file.write(
f"""[Unit]
Description=Python script for synchronizing repositories

[Service]
Type=oneshot
WorkingDirectory={cwd}
ExecStart={cwd}/reposync.py""")
service_file.close()

timer_file = open(r"/etc/systemd/system/reposync.timer", "w")
timer_file.write(
f"""[Unit]
Description=Python script for synchronizing repositories

[Timer]
OnBootSec=10min
OnUnitActiveSec=10min

[Install]
WantedBy=timers.target""")
timer_file.close()

print(
"""reposync.service and reposync.timer files created successfully!
You'll now need to run the following to activate them (as root): systemctl daemon-reload && systemctl start reposync.timer
To remove them, execute (as root) this: rm -f /etc/systemd/system/reposync.*""")
