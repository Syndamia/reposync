#!/usr/bin/python3
from git import Repo
from datetime import datetime
import os 

repoFile = open("repositories.txt", "r")

while(True):
	line = repoFile.readline().strip()
	if not line:
		break

	now = datetime.now().strftime("[%H:%M:%S] ")
	if not os.path.isdir(line):
		print("\033[0;31m%s\033[0;0m \033[0;34m%s\033[0;0m is not a directory!"%(now, line))
		continue

	repo = Repo(line)
	for remote in repo.remotes:
		remote.fetch()

	if not repo.bare:
		branch = repo.active_branch

		# Thanks to https://stackoverflow.com/a/65535263/12036073
		if list(repo.iter_commits(f'{branch}..{branch}@{{u}}')):
			print(f'\033[0;31m{now}\033[0;34m{line}\033[0;0m\'s origin was updated, pulling...')
			repo.remotes.origin.pull()
		else:
			print(f'\033[0;32m{now}\033[0;34m{line}\033[0;0m is up to date')
	else:
		print(f'\033[0;31m{now}\033[0;0m Repository \033[0;34m{line}\033[0;0m must not be bare!')

repoFile.close()
