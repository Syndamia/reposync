# RepoSync

A simple Python script with which you can synchronize chosen git repositories.

### Dependencies

1. `Python`: you need to install Python, version **3.6** or later
   - Debian and Ubuntu: `sudo apt install python3-pip`
2. `Pip3`: it should come with you install of Python, but check if you have it, just in case
3. `GitPython`: this is the git module via which the script executes git commands
    - You install it with pip: `sudo -H pip3 install GitPython`

## reposync.py

This is the main script, it looks at the repositories inside `repositories.txt`, fetches them and if there are any updates to the **active branch**'s remote: it pulls.

It is really meant to be used with some sort of [job scheduler](https://en.wikipedia.org/wiki/Job_scheduler), since it runs only once through each and every repo inside `repositories.txt`.

**Note:** the script checks for the `repositories.txt` file inside the same directory and extracts repository locations from that. If you want to change the file location or name, just change `Line 6` inside the file (replace "repositories.txt" with your path of choice, for example "/home/admin/repositories-to-update.txt").

## repositories.txt

This is the file in which all repository locations are listed. On each line, you should have a relative or absolute path of a git repository and that's it.

Each time the script is ran, it opens this file, gets every line and tries to fetch from the directory (specified by each line). To remove a repository, just delete the line with it's path.

## systemd-service-create.py

A helper script that creates a sample systemd service and timer inside `/etc/systemd/system/`. The service just executes the script (and finishes after the script has went through all repositories), while the timer executes the service over a given interval.

You wouldn't need to run it more than once. Don't worry if you see a message along the lines of `WARNING: if you use SELinux, you'll need to ...`, it's just a note that always gets shown.

After it finishes, remember to read the final output! It states that you'll need to execute the following command:
```bash
# systemctl daemon-reload && systemctl start reposync.timer
```
